package com.nori.Core.domain;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.type.Alias;

@Alias("user")
public class User{
	
	private static final long serialVersionUID = -8369712541698922901L;
	
	
	private int id;
	
	private String userId;

	private String password;
	
	private String name;				//이름
	
	private String birthday;			//생년월일
	
	private String phone;			//연락처
	
	private String email;			//이메일
	
	private String zipcode;			//우편번호
	
	private String address;			//주소
	
	private String detailAddress;	//상세주소
	
	private Date modifyDate;		//수정일
	
	private String modifyUserId;		//수정자
	
	private UserAccessLog userAccessLog; 	//AcessLog                        

	private List<UserRoles> userRoleList;	//권한
	
	private Date regDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public UserAccessLog getUserAccessLog() {
		return userAccessLog;
	}

	public void setUserAccessLog(UserAccessLog userAccessLog) {
		this.userAccessLog = userAccessLog;
	}

	public List<UserRoles> getUserRoleList() {
		return userRoleList;
	}

	public void setUserRoleList(List<UserRoles> userRoleList) {
		this.userRoleList = userRoleList;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	

	
}
