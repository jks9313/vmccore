/*
 * Usage Ex)
 * @RetryConcurrentOperation(exception = com.mysql.jdbc.exceptions.jdbc4.MySQLTransactionRollbackException.class, retries = 4, initialDelay=100, maximumDelay=3000)
 */
package com.nori.Core.tx;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface RetryConcurrentOperation {

	long DEFAULT_INITIAL_DELAY = 100;

	long DEFAULT_MAXIMUM_DELAY = 30 * 1000;

	@SuppressWarnings("rawtypes")
	Class exception() default Exception.class;

	int retries() default -1;

	long initialDelay() default -1;

	long maximumDelay() default -1;
}
