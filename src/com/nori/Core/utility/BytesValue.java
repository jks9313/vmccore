package com.nori.Core.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BytesValue extends Number implements BytesInOutStream {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5869494405014852264L;


	static enum ENDIAN{
        BIG_ENDIAN,
        LIITLE_ENDIAN
    }

    private byte[] bytes;
    private ENDIAN endian;
    private int byteSize;

    public BytesValue(int byteSize) {
        this.byteSize = byteSize;
        this.bytes = new byte[byteSize];
        this.endian = ENDIAN.BIG_ENDIAN;
    }

    public void setOrder(ENDIAN endian)
    {
        byte[] temp = this.bytes;
        if(this.endian!=endian)
        {
            temp=Util.reverse(this.bytes);
        }
        this.endian = endian;
        this.bytes = temp;
    }





    public byte[] getBytes(ENDIAN endian) {


        byte[] temp = this.bytes;
        if(this.endian!=endian)
        {
            temp=Util.reverse(this.bytes);
        }

        return temp;
    }

    public void setBytes(byte[] bytes, ENDIAN endian) {

        byte[] temp = bytes;
        if(this.endian!=endian)
        {

            temp=Util.reverse(this.bytes);

        }
        System.arraycopy(temp, 0, this.bytes,0 ,(int) this.byteSize);
    }


    public void setByte(byte value)
    {
        this.bytes[0]=value;
    }

    public void setShort(short value)
    {
        int size=Short.SIZE;
        byte[] temp = new byte[this.byteSize];
        for(int i=0;i<this.byteSize && i<size;i++)
        {
            temp[i] = (byte)((value >> (8*i)) & 0xff);
        }
        if(this.endian == ENDIAN.BIG_ENDIAN)
        {
            temp = Util.reverse(temp);
        }

        this.bytes = temp;


    }

    public void setInt(int value)
    {
        int size=Integer.SIZE;
        byte[] temp = new byte[this.byteSize];
        for(int i=0;i<this.byteSize && i<size;i++)
        {
            temp[i] = (byte)((value >> (8*i)) & 0xff);
        }
        if(this.endian == ENDIAN.BIG_ENDIAN)
        {
            temp = Util.reverse(temp);
        }

        this.bytes = temp;
    }


    public void setLong(long value)
    {
        int size=Long.SIZE;
        byte[] temp = new byte[this.byteSize];
        for(int i=0;i<this.byteSize && i<size;i++)
        {
            temp[i] = (byte)((value >> (8*i)) & 0xff);
        }
        if(this.endian == ENDIAN.BIG_ENDIAN)
        {
            temp = Util.reverse(temp);
        }

        this.bytes = temp;
    }

    public void setFloat(float value)
    {
        int size=Float.SIZE;
        long valueTwo = Float.floatToRawIntBits(value);

        byte[] temp = new byte[this.byteSize];
        for(int i=0;i<this.byteSize && i<size;i++)
        {
            temp[i] = (byte)((valueTwo >> (8*i)) & 0xff);
        }
        if(this.endian == ENDIAN.BIG_ENDIAN)
        {
            temp = Util.reverse(temp);
        }

        this.bytes = temp;



    }
    public void setDouble(Double value)
    {
        int size=Double.SIZE;
        long valueTwo = Double.doubleToRawLongBits(value);

        byte[] temp = new byte[this.byteSize];
        for(int i=0;i<this.byteSize && i<size;i++)
        {
            temp[i] = (byte)((valueTwo >> (8*i)) & 0xff);
        }
        if(this.endian == ENDIAN.BIG_ENDIAN)
        {
            temp = Util.reverse(temp);
        }

        this.bytes = temp;
    }











    @Override
    public byte byteValue() {
        byte[] temp = this.bytes;
        if(this.endian==ENDIAN.BIG_ENDIAN)
        {

            temp=Util.reverse(this.bytes);

        }



        return (byte)(temp[0] & 0xff);
    }

    @Override
    public short shortValue() {
        byte[] temp = this.bytes;
        if(this.endian==ENDIAN.BIG_ENDIAN)
        {

            temp=Util.reverse(this.bytes);

        }

        long ret=0;
        for(int i=0;i<this.byteSize;i++)
        {
            ret |= ((temp[i]& 0xff)<< (8*i));

        }


        return (short)ret;
    }


    @Override
    public double doubleValue() {

        byte[] temp = this.bytes;
        if(this.endian==ENDIAN.BIG_ENDIAN)
        {

            temp=Util.reverse(this.bytes);

        }

        long ret=0;
        for(int i=0;i<this.byteSize;i++)
        {
            ret |= ((temp[i]& 0xff)<< (8*i));

        }


        return Double.longBitsToDouble(ret);
    }

    @Override
    public float floatValue() {
        byte[] temp = this.bytes;
        if(this.endian==ENDIAN.BIG_ENDIAN)
        {

            temp=Util.reverse(this.bytes);

        }

        long ret=0;
        for(int i=0;i<this.byteSize;i++)
        {
            ret |= ((temp[i]& 0xff)<< (8*i));

        }


        return Float.intBitsToFloat((int)ret);
    }


    @Override
    public int intValue() {
        byte[] temp = this.bytes;
        if(this.endian==ENDIAN.BIG_ENDIAN)
        {

            temp=Util.reverse(this.bytes);

        }

        long ret=0;
        for(int i=0;i<this.byteSize;i++)
        {

            ret |= ((temp[i]& 0xff)<< (8*i));

        }


        return (int)ret;
    }

    @Override
    public long longValue() {
        byte[] temp = this.bytes;
        if(this.endian==ENDIAN.BIG_ENDIAN)
        {

            temp=Util.reverse(this.bytes);

        }

        long ret=0;
        for(int i=0;i<this.byteSize;i++)
        {
            ret |= ((temp[i]& 0xff)<< (8*i));

        }


        return ret;
    }


    @Override
    public int getByteCount() {

        return this.byteSize;
    }


    @Override
    public void writeOutputStream(OutputStream output,int offset, int lenth) throws IOException
    {
        output.write(this.bytes, offset, lenth);
    }


    @Override
    public int  readInputStream(InputStream input,int offset, int lenth) throws IOException
    {

        int readCount = input.read(this.bytes, offset, lenth);
        return readCount;
    }

}
