//package com.nori.Core.utility;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.util.Iterator;
//
//import org.apache.poi.hssf.usermodel.HSSFFont;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellStyle;
//import org.apache.poi.ss.usermodel.CellType;
//import org.apache.poi.ss.usermodel.DataFormatter;
//import org.apache.poi.ss.usermodel.HorizontalAlignment;
//import org.apache.poi.ss.usermodel.IndexedColors;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.VerticalAlignment;
//import org.apache.poi.ss.usermodel.DateUtil;
//import org.apache.poi.ss.util.CellReference;
//import org.apache.poi.xssf.usermodel.XSSFCellStyle;
//import org.apache.poi.xssf.usermodel.XSSFFont;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.springframework.beans.factory.annotation.Autowired;
//
//public class PoiExcelFormatUtil {
//
//	public static CellStyle createBorderedStyle(XSSFCellStyle style) {
//		style.setBorderRight(CellStyle.BORDER_THIN);
//		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
//		style.setBorderBottom(CellStyle.BORDER_THIN);
//		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
//		style.setBorderLeft(CellStyle.BORDER_THIN);
//		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
//		style.setBorderTop(CellStyle.BORDER_THIN);
//		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
//		return style;
//	}
//
//	public static XSSFFont getFont(XSSFWorkbook workbook, String name, int size, boolean bold, boolean italic, boolean underline) {
//		XSSFFont font = workbook.createFont();
//		font.setFontName(name);
//		font.setFontHeightInPoints((short) size);
//		font.setBold(bold);
//		font.setItalic(italic);
//		font.setUnderline(HSSFFont.U_NONE);
//		if (underline) {
//			font.setUnderline(HSSFFont.U_SINGLE);
//		}
//		return font;
//	}
//
//	public static XSSFCellStyle createStyle(XSSFWorkbook workbook, HorizontalAlignment horizontal, VerticalAlignment vertical, XSSFFont font, short color, boolean wrap) {
//
//		/*XSSFCellStyle style = workbook.createCellStyle();
//		style.setBorderTop(BorderStyle.THIN);
//		style.setBorderLeft(BorderStyle.THIN);
//		style.setBorderBottom(BorderStyle.THIN);
//		style.setBorderRight(BorderStyle.THIN);
//		style.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
//		style.setAlignment(HorizontalAlignment.CENTER);
//		style.setVerticalAlignment(VerticalAlignment.CENTER);
//		style.setFont(font);*/
//
//		XSSFCellStyle style = workbook.createCellStyle();
//		createBorderedStyle(style);
//		style.setFillForegroundColor(color);
//		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
//		//style.setFillBackgroundColor(color);
//		style.setAlignment(horizontal);
//		style.setVerticalAlignment(vertical);
//		style.setFont(font);
//		style.setWrapText(wrap);
//
//		return style;
//	}
//	
//	@Autowired
//	public static void insertRow(File file) {
//
//		try {
//			FileInputStream fis = new FileInputStream(file);
//		
//			
//			/* Load workbook */
//			@SuppressWarnings("resource")
//			XSSFWorkbook workbook = new XSSFWorkbook(fis);
//			
//			/* Load worksheet */
//			XSSFSheet sheet = workbook.getSheetAt(0);
//			
//			// we loop through and insert data
//			Iterator<Row> ite = sheet.rowIterator();
//			
//			
//			
//			
//			DataFormatter formatter = new DataFormatter();
//		    Sheet sheet1 = workbook.getSheetAt(0);
//		    for (Row row : sheet1) {
//		        for (Cell cell : row) {
//		            CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());
//		            System.out.print(cellRef.formatAsString());
//		            System.out.print(" - ");
//
//		            // get the text that appears in the cell by getting the cell value and applying any data formats (Date, 0.00, 1.23e9, $1.23, etc)
//		            String text = formatter.formatCellValue(cell);
//		            
//
//		            
//		            if(cell.getCellTypeEnum().equals( CellType.STRING)){
//		            	
//		            }else if(cell.getCellTypeEnum().equals( CellType.NUMERIC)){
//		            	if (DateUtil.isCellDateFormatted(cell)) {
//	                        
//	                    } else {
//	                        
//	                    }
//		            }else if(cell.getCellTypeEnum().equals( CellType.BOOLEAN)){
//		            	  
//		            }else if(cell.getCellTypeEnum().equals( CellType.FORMULA)){
//		            	
//		            }else if(cell.getCellTypeEnum().equals( CellType.BLANK)){
//		            	
//		            }else{
//		            	
//		            }
//		            
//		            // Alternatively, get the value and format it yourself
//		            /*switch (cell.getCellTypeEnum()) {
//		                case CellType.STRING:
//		                    
//		                    break;
//		                case CellType.NUMERIC:
//		                    if (DateUtil.isCellDateFormatted(cell)) {
//		                        
//		                    } else {
//		                        
//		                    }
//		                    break;
//		                case CellType.BOOLEAN:
//		                    
//		                    break;
//		                case CellType.FORMULA:
//		                    
//		                    break;
//		                case CellType.BLANK:
//		                    
//		                    break;
//		                default:
//		                    
//		            }*/
//		        }
//		    }
//
//
//			/*while (ite.hasNext()) {
//				Row row = (Row) ite.next();
//				
//				Iterator<Cell> cellIterator = row.cellIterator();
//				
//
//				while (cellIterator.hasNext()) {
//
//					Cell cell = cellIterator.next();
//
//					switch (cell.getCellType()) {
//					case Cell.CELL_TYPE_STRING: // handle string columns
//						
//						break;
//					case Cell.CELL_TYPE_NUMERIC: // handle double data
//						int i = (int) cell.getNumericCellValue();
//						
//						break;
//					}
//				    
//					
//					
//				}
//			}*/
//			/* Close input stream */
//			fis.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
////		public static void insertRow(String filePath) {
////			
////			try {
////				FileInputStream fis = new FileInputStream(new File(filePath));
////				
////				
////				/* Load workbook */
////				@SuppressWarnings("resource")
////				XSSFWorkbook workbook = new XSSFWorkbook(fis);
////				
////				/* Load worksheet */
////				XSSFSheet sheet = workbook.getSheetAt(0);
////				
////				// we loop through and insert data
////				Iterator<Row> ite = sheet.rowIterator();
////				
////				
////				while (ite.hasNext()) {
////					Row row = (Row) ite.next();
////					
////					Iterator<Cell> cellIterator = row.cellIterator();
////					
////					
////					while (cellIterator.hasNext()) {
////						
////						Cell cell = cellIterator.next();
////						
////						switch (cell.getCellType()) {
////						case Cell.CELL_TYPE_STRING: // handle string columns
////							
////							break;
////						case Cell.CELL_TYPE_NUMERIC: // handle double data
////							int i = (int) cell.getNumericCellValue();
////							
////							break;
////						}
////						
////					}
////				}
////				/* Close input stream */
////				fis.close();
////			} catch (IOException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
//
//	}
//}
