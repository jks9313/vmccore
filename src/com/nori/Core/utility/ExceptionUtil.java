package com.nori.Core.utility;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.springframework.web.util.NestedServletException;

public class ExceptionUtil {

	public static String getMessage(Throwable ex) {
		StringBuilder output = new StringBuilder();
		if (ex instanceof NestedServletException) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.getCause().printStackTrace(pw);
			pw.close();
			output.append(sw.toString());
		} else {
			if (ex.getCause() != null && ex.getCause().getMessage() != null) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.getCause().printStackTrace(pw);
				pw.close();
				output.append(sw.toString());
			} else {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.printStackTrace(pw);
				pw.close();
				output.append(sw.toString());
			}
		}

		return output.toString();
	}
}
