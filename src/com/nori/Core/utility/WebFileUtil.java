package com.nori.Core.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


@Component
public class WebFileUtil {
	
	public static String fileStorePath;
	public static String nasRootPath;
//	private static String filePath	=	"/Users/kwonjo/Desktop/acpWorkspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/upload/";	
	private static String filePath	=	"/Users/kwonjo/Desktop/primeWorkspace/importData/";	
		
	// image value : String -> map
		public static SortedMap getImgList(String str) throws JsonParseException, JsonMappingException, IOException {
			
			ObjectMapper mapper = new ObjectMapper();
			SortedMap<String, String> map = new TreeMap<String, String>();	 
			
			if(!"".equals(str)){			
				map = mapper.readValue(str, new TypeReference<TreeMap<String, String>>() {});			
			}			
			return map;
		}
		
		// Goods Image Multi Upload
		public static String imageMultiUpload(String chk, String img, String goods_id, MultipartFile[] file, String str) throws Exception {
			
			String returnStr = "";
			
			if(!"".equals(chk.replace(",",""))){
				SortedMap<String, String> map = new TreeMap<String, String>();
		    	ObjectMapper om = new ObjectMapper();
				String destDir = fileStorePath+str+"/"+goods_id;
				
				File repositoryDir = new File(destDir);
				if (!repositoryDir.exists()) {
					boolean created = repositoryDir.mkdirs();
					if (!created) {
						throw new Exception("Fail to create a directory for attached file ["+ repositoryDir + "]");
					}
				}			
				
				int index = 1;
				//기존 이미지 데이터 map 담기
				if(!"".equals(img)){
					ObjectMapper mapper = new ObjectMapper();
					map = mapper.readValue(img, new TypeReference<TreeMap<String, String>>() {});
					
					Iterator<Entry<String, String>> iter = map.entrySet().iterator();
					
					while (iter.hasNext()) {
						boolean input = true;
						Entry entry = (Entry) iter.next();	
						map.put("img_"+index,(String) entry.getValue());
						index++;
					}				
				}
				
				//이미지업로드 , 이미지 데이터 추가
				for(int i = 0; i < file.length; i++){
					if(!"".equals(file[i].getOriginalFilename())){
						Random r = new Random(); 
						String rand_val = Integer.toString(r.nextInt(999999));		
						String realFileName = file[i].getOriginalFilename();
						String fileNameExtension = realFileName.substring(realFileName.lastIndexOf(".")).toLowerCase();
						String fileId = WebUtil.getTodayDate(14) + rand_val;		
						String convertedFileName = fileId + fileNameExtension;
						file[i].transferTo(new File(destDir + "/" + convertedFileName));
						map.put("img_"+index, "/upload/"+str+"/"+goods_id+"/"+convertedFileName);					
						index++;
					}				
				}
				
				if(!map.isEmpty()){
					returnStr = om.defaultPrettyPrintingWriter().writeValueAsString(map);
				}
			}
			return returnStr;			
		}
		
		/**
		 * 임시파일 저장후 -> 파일체크섬추출후 체크섬을 파일이름으로 사용(임시파일 rename) -> 만약 동일한체크섬의 파일이름이 존재하면 임시파일 삭제.
		 * @param destPath
		 * @param file
		 * @return 생성된 파일명
		 */
		public static String uploadSingleFileByChecksum(String destPath, MultipartFile file)  {
			if(file != null && !file.isEmpty()){
				File repositoryDir = new File(destPath);
				if (!repositoryDir.exists()) {
					if ( ! repositoryDir.mkdirs() ) {
						throw new RuntimeException("Fail to create a directory for attached file ["+ repositoryDir + "]");
					}
				}    		
				
				Random r = new Random(); 
				String rand_val = Integer.toString(r.nextInt(999999));		
				String realFileName = file.getOriginalFilename();
				String fileNameExtension = realFileName.substring(realFileName.lastIndexOf(".")).toLowerCase();
				String fileId = WebUtil.getTodayDate(14) + rand_val;		
				String convertedFileName = "temp_"+fileId + fileNameExtension;
				
				String tempSavedFullPath = destPath + "/" + convertedFileName;
				File tempSavedFile = new File(tempSavedFullPath);
				try {
					file.transferTo(tempSavedFile);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}	
				String checksum = verifyChecksum(tempSavedFile);
				
				String destConvertedFileName = checksum + fileNameExtension;
				String destSavedFullPath = destPath + "/" + destConvertedFileName;
				File destFile = new File(destSavedFullPath);
				if( destFile.exists() ){
					tempSavedFile.delete();
				}else{
					if( ! tempSavedFile.renameTo(destFile) ){
						throw new RuntimeException("Fail to save file ["+ repositoryDir + "]");
					}
				}
				return destFile.getName();
				
			}
			return null;
		}
		
		// Image Single Upload
		public static String imageSingleUpload(String id, MultipartFile file, String str) throws Exception {
			
			String returnStr = "";
			String destDir = "";
			if(!file.isEmpty()){
				
				destDir = fileStorePath+str+"/"+id;
				/*if("category".equals(str) || "tvtheme".equals(str) || "event".equals(str) || "popup".equals(str)) {
					destDir = fileStorePath+str;
				}*/
				
				File repositoryDir = new File(destDir);
				if (!repositoryDir.exists()) {
					boolean created = repositoryDir.mkdirs();
					if (!created) {
						throw new Exception("Fail to create a directory for attached file ["+ repositoryDir + "]");
					}
				}    		
				
				Random r = new Random(); 
				String rand_val = Integer.toString(r.nextInt(999999));		
				String realFileName = file.getOriginalFilename();
				String fileNameExtension = realFileName.substring(realFileName.lastIndexOf(".")).toLowerCase();
				String fileId = WebUtil.getTodayDate(14) + rand_val;		
				String convertedFileName = fileId + fileNameExtension;
				file.transferTo(new File(destDir + "/" + convertedFileName));		
						
				returnStr = "/upload/"+str+"/"+id+"/"+convertedFileName;
				
				/*if("category".equals(str) || "tvtheme".equals(str) || "event".equals(str) || "popup".equals(str)) {
					returnStr = "/upload/"+str+"/"+convertedFileName;
				}*/
			}
			
			return returnStr;
		}
		
		// File Single Upload
		public static String[] fileSingleUpload(MultipartFile file, String folderName) throws Exception {
			if(!file.isEmpty()){
				String destDir = fileStorePath + folderName;
				File repositoryDir = new File(destDir);
				if(!repositoryDir.exists()) {
					if(!repositoryDir.mkdirs()) {
						throw new Exception("Fail to create a directory for attached file ["+ repositoryDir + "]");
					}
				}

				Random r = new Random();
				String rand_val = Integer.toString(r.nextInt(999999));
				String realFileName = file.getOriginalFilename();
				String fileNameExtension = realFileName.substring(realFileName.lastIndexOf(".")).toLowerCase();
				String fileId = WebUtil.getTodayDate(14) + rand_val;
				String convertedFileName = fileId + fileNameExtension;
				file.transferTo(new File(destDir + "/" + convertedFileName));

				String[] returnStr = new String[3];
				returnStr[0] = "/upload/" + folderName + "/";
				returnStr[1] = convertedFileName;
				returnStr[2] = realFileName;
				return returnStr;
			}
			return null;
		}
				
		/**
	     * Verifies file's SHA1 checksum
	     */
	    public static String verifyChecksum(File file) {
	    	FileInputStream fis = null;
	    	StringBuffer sb = new StringBuffer();
	    	try {
	    		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
	            fis = new FileInputStream(file);
	      
	            byte[] data = new byte[1024*10];
	            int read = 0; 
	            while ((read = fis.read(data)) != -1) {
	                sha1.update(data, 0, read);
	            };
	            byte[] hashBytes = sha1.digest();
	      
	            
	            for (int i = 0; i < hashBytes.length; i++) {
	              sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	             
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				if(fis != null){try {	fis.close();	} catch (IOException e) {	e.printStackTrace();	}}
			}
	        
	        return sb.toString();
	    }
	 
	    /***
	     * 
	     * @param multipart
	     * @return file
	     * @throws IllegalStateException
	     * @throws IOException
	     */
	    public static File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException 
	    {
	        File convFile = new File(filePath + multipart.getOriginalFilename());
	        multipart.transferTo(convFile);
	        return convFile;
	    }
}
