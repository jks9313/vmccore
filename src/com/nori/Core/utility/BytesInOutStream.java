package com.nori.Core.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface BytesInOutStream {

	public int getByteCount();
    public void writeOutputStream(OutputStream output,int offset, int lenth) throws IOException;
    public int  readInputStream(InputStream input,int offset, int lenth) throws IOException;
}
