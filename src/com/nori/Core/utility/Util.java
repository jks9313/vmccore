package com.nori.Core.utility;

public class Util {

    public static byte[] reverse(byte[] objects)
    {
        byte[] temp = new byte[objects.length];
        for (int left = 0, right = objects.length - 1; left <= right; left++, right--) {
            temp[left]=objects[right];
            temp[right]=objects[left];
        }
        return temp;
    }
}