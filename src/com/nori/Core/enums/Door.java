package com.nori.Core.enums;

public enum Door {

	OPEN((String)"0001", "열림"),
	CLOSE((String)"0002", "닫힘"),
	CHECK((String)"0003", "상태확인요");
	
	private String code;
	private String value;

	Door(String code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return code;
	}

	public static Door toEnum(String code) {
		for (Door keyData : Door.values()) {
			if (keyData.getCode().equalsIgnoreCase(code)) {
				return keyData;
			}
		}
		return CHECK;
	}
	
	
	public static Door fromValue(String value) {
		for (Door keyData : Door.values()) {
			if (keyData.value.equalsIgnoreCase(value)) {
				return keyData;
			}
		}
		return CHECK;
	}
}
