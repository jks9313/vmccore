package com.nori.Core.enums;

public enum AlarmRamp {

	EMERGENCY((String)"0001", "비상"),
	ABNORMAL((String)"0002","비정상"),
	CLOSE((String)"0003", "정상"),
	MEASURE((String)"0004", "의무조치"),
	NEUTRALITY((String)"0005", "중립"),
	Unknown((String)"0000", "Unknown");
	
	private String code;
	private String value;

	AlarmRamp(String code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return code;
	}

	public static AlarmRamp toEnum(String code) {
		for (AlarmRamp keyData : AlarmRamp.values()) {
			if (keyData.getCode().equalsIgnoreCase(code)) {
				return keyData;
			}
		}
		return Unknown;
	}
	
	
	public static AlarmRamp fromValue(String value) {
		for (AlarmRamp keyData : AlarmRamp.values()) {
			if (keyData.value.equalsIgnoreCase(value)) {
				return keyData;
			}
		}
		return Unknown;
	}
}
