package com.nori.Core.view;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.view.AbstractView;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonResponseView extends AbstractView {
	
	private static Logger logger = Logger.getLogger(JsonResponseView.class);
	
	@Autowired
	private ObjectMapper objectMapper;

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		JsonResponse jsonResponse = (JsonResponse) model.get("jsonResponse");
		if (jsonResponse == null) {
			jsonResponse = new JsonResponse();

			jsonResponse.setStatus(HttpStatus.valueOf(response.getStatus()));

			jsonResponse.setSuccess(false);

			jsonResponse.setMessage(HttpStatus.valueOf(response.getStatus()).getReasonPhrase());
		}
		PrintWriter out = null;
		try {
			response.resetBuffer();
			response.reset();
			
			response.setStatus(jsonResponse.getStatus().value());
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json; charset=UTF-8");
			out = new PrintWriter(new BufferedWriter(response.getWriter(), 12288));
			out.print(objectMapper.writeValueAsString(jsonResponse));
			out.flush();
		} catch (java.io.IOException ioe) {
			logger.fatal(ioe, ioe);
			throw ioe;
		} catch (Exception ex) {
			logger.fatal(ex, ex);
			throw new ServletException(ex);
		} finally {
			try {
				if (out != null) {
					out.flush();
				}
			} catch (Exception ex) {
			} finally {
				out = null;
			}
		}
	}
}