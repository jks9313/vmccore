package com.nori.Core.view;

import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.view.AbstractView;

import com.nori.Core.utility.Base64;
import com.nori.Core.utility.StringUtil;

public class Base64DecodingImageView extends AbstractView {

	private static Logger logger = Logger.getLogger(Base64DecodingImageView.class);
	
	private String mime = "image/jpeg";

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String image = (String) model.get("image");
		if (StringUtil.isEmptyString(image)) {
			byte[] imageData = null;
			OutputStream fout = null;
			try {
				imageData = Base64.Decode(image);
				response.reset();
				response.setContentType(mime);
				response.setHeader("Content-Length", "" + imageData.length);
				fout = response.getOutputStream();
				fout.write(imageData, 0, imageData.length);
				fout.flush();
			} catch (Exception e) {
				logger.fatal(e, e);
				throw e;
			} finally {
				try {
					if (fout != null) {
						fout.close();
					}
				} catch (Exception ex) {
				} finally {
					fout = null;
				}
				imageData = null;
			}
		}
	}
}
